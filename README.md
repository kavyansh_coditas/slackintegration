# Slack Integration
- Sample flask app that will require `Slack_Token, Channel-ID and Message` for pushing message on Slack.
- pip install -r requirements.txt
- Run app.py


## Please Note 
- you have added your slack-bot in the respected channel where you want to post message.
- Slack-Token will generate during creation of Slack-APP.(It will not change and unique for every Slack-APP)


## Updated
- for using same slack-app, kindly check [Integration_using_static_creds](https://gitlab.com/kavyansh_coditas/slackintegration/-/tree/Integration_using_static_creds)
