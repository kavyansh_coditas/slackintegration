from flask import Blueprint, request, render_template
import slack

PushingMessage = Blueprint("PushingMessage",__name__, template_folder="templates")

@PushingMessage.route("/", methods= ['POST', 'GET'])
def homepage():
    if request.method == 'POST':
        channel_id = request.form['channel_id']
        message = request.form['message']
        token = request.form['token']

        slackClient = slack.WebClient(token=token)

        result = slackClient.chat_postMessage(
            channel = channel_id,
            text=message
        )

        print(result)

        return render_template("index.html")


    else:
        return render_template("index.html")    